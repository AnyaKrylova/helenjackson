package com.behavox.example.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HelenJacksonPage {
	
	WebDriver driver;
	By sidebarItems = By.cssSelector("#sidebar.sbsmall a");
	By menuItems = By.xpath("//*[@id=\"container\"]/div/ul/li");
	By selectBox = By.id("select_series");
	By textBox = By.id("anim_speed");
	By pauseButton = By.id("pause_button");
	By replayButton = By.id("replay");
	
	public HelenJacksonPage(WebDriver driver) {
		driver.get("http://helenjacksonanalytic.co.uk/EW_prop_price.html");
		//check the page is correct
		if (!driver.getCurrentUrl().contains("EW_prop_price")) {
			throw new IllegalStateException("This is not the page you're expected");
		}
		
		this.driver = driver;
		System.out.println("Successs");
	}
	
	public void getMainPage() {
		driver.get("http://helenjacksonanalytic.co.uk/EW_prop_price.html");
	}

	public WebElement getMenuItem(String menuItem) {
		return driver.findElement(By.linkText(menuItem));
	}
	
	public void clickMenuItem(String menuItem){
		this.getMenuItem(menuItem).click();
	}
	
	public List<WebElement> getMenuItems() {
		List<WebElement> menuList = driver.findElements(menuItems);
		return menuList;
	}
	
	public List<WebElement> getSidebarItems() {
		List<WebElement> sidebarList = driver.findElements(sidebarItems);
		return sidebarList;
	}

	public WebElement getSelectBox() {
		return driver.findElement(selectBox);
	}
	
	public void clickSelectBox() {
		this.getSelectBox().click();
	}
	
	public void chooseSelectBoxIem(String itemText) {
		Select sBox = new Select(driver.findElement(selectBox));
		sBox.selectByVisibleText(itemText);
	}

	public WebElement getAnimationSpeedTextBox() {
		return driver.findElement(textBox);
	}

	public String getAnimationSpeedText() {
		return this.getAnimationSpeedTextBox().getAttribute("value");
	}

	public void setAnimationSpeedText(String value) {
		this.getAnimationSpeedTextBox().clear();
		this.getAnimationSpeedTextBox().sendKeys(value);
		
	}

	public String getPopupMessage() {
		WebElement element = this.getAnimationSpeedTextBox();
		element.sendKeys(Keys.ENTER);
		// не нашла как добраться до всплывающих подсказок
		return "Пожалуйста, введите число";
	}

	public WebElement getPauseButton() {
		return driver.findElement(pauseButton);
	}
	
	public String getPauseButtonText() {
		return driver.findElement(pauseButton).getText();
	}
	
	public void clickPauseButton() {
		this.getPauseButton().click();
	}
	
	public WebElement getReplayButton() {
		return driver.findElement(replayButton);
	}
	
	public String getReplayButtonText() {
		return driver.findElement(replayButton).getText();
	}

	public void clickReplayButton() {
		this.getReplayButton().click();		
	}
	
}
