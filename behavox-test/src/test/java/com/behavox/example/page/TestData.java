package com.behavox.example.page;

import java.util.HashMap;

public class TestData {
	public static final String PATH_TO_DRIVER = "C:\\chromedriver_win32\\chromedriver.exe";
	
	public static final String[] SIDEBAR_ITEMS = {
			"England and Wales property prices",
			"UK economy as a network",
			"Bottlenecks in EU LNG supply",
			"Millennium Development Goals",
			"Schelling model of urban segregation",
			"Weather and agricultural yields",
			"Dynamic fisheries model",
			"Visualising sensitivity analysis"
	};
	
	public static final String[] MENU_ITEMS = {
			"Home",
			"CV",
			"Working with clients",
			"Example analyses",
			"Contact",
	};
	
	public static HashMap<String, String> setMenuItemsValues() {
		HashMap<String, String> menuItems = new HashMap <String, String>();
		menuItems.put("Home", "index.html");
		menuItems.put("CV", "cv.html");
		menuItems.put("Working with clients", "working-with-clients.html");
		menuItems.put("Example analyses", "example-analyses.html");
		menuItems.put("Contact", "contact.html");
		return menuItems;
	}
	
	public static HashMap<String, String> setSelectBoxValues() {
		HashMap<String, String> selectBoxValues = new HashMap<String, String>();
		selectBoxValues.put("Nominal prices", 
				"England and Wales mean property prices by district, at nominal prices, 1995-2015");
		selectBoxValues.put("Current prices", 
				"England and Wales mean property prices by district, at current prices, 1995-2015");
		selectBoxValues.put("Compared with 1995", 
				"England and Wales mean property prices by district 1995-2015, as a multiple of 1995 values");
		selectBoxValues.put("Annual percentage increase", 
				"Mean annual percentage increase in England and Wales property prices by district, 1996-2014");
		selectBoxValues.put("Compared with local earnings", 
				"England and Wales mean property prices by district, as a multiple of mean local annual full-time earnings, 1997-2015");
		return selectBoxValues;
	}
}
