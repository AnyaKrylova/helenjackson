package com.behavox.example.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.behavox.example.page.HelenJacksonPage;
import com.behavox.example.page.TestData;

import static com.behavox.example.page.TestData.PATH_TO_DRIVER;

public class CheckSelectBox {
	
	static WebDriver driver;
	static HelenJacksonPage helenJacksonPage;
	
	@BeforeClass
	public static void setUp(){
		System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		helenJacksonPage = new HelenJacksonPage(driver);
	}
	
	@Before
	public void toMainPage(){
		helenJacksonPage.getMainPage();
	}
	
	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void checkSelectBoxItemLeadsToExpectedPage() {
		HashMap<String, String> selectBoxValues = TestData.setSelectBoxValues();
		
		for (HashMap.Entry<String, String> item : selectBoxValues.entrySet()) {
			helenJacksonPage.clickSelectBox();
			helenJacksonPage.chooseSelectBoxIem(item.getKey());
			assertTrue(driver.getPageSource().contains(item.getValue()));
		}
	}

}
