package com.behavox.example.tests;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.behavox.example.page.HelenJacksonPage;
import static com.behavox.example.page.TestData.PATH_TO_DRIVER;

public class CheckButtons {
	
	
	static WebDriver driver;
	static HelenJacksonPage helenJacksonPage;
	
	@BeforeClass
	public static void setUp(){
		System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		helenJacksonPage = new HelenJacksonPage(driver);
	}
	
	@Before
	public void toMainPage(){
		helenJacksonPage.getMainPage();
	}
	
	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void checkPauseButton() {
		helenJacksonPage.clickPauseButton();
		assertEquals("Resume", helenJacksonPage.getPauseButtonText());
		
		helenJacksonPage.clickPauseButton();
		assertEquals("Pause", helenJacksonPage.getPauseButtonText());
	}

	@Test
	public void checkReplayButton() {
		helenJacksonPage.clickPauseButton(); // to check it will be changed after Replay 
		helenJacksonPage.clickReplayButton();
		
		assertEquals("Replay", helenJacksonPage.getReplayButtonText());
		assertEquals("Pause", helenJacksonPage.getPauseButtonText());
	}
}
