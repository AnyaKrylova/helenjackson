package com.behavox.example.tests;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.behavox.example.page.HelenJacksonPage;
import static com.behavox.example.page.TestData.PATH_TO_DRIVER;

public class CheckTextBox {
	
	static WebDriver driver;
	static HelenJacksonPage helenJacksonPage;
	
	@BeforeClass
	public static void setUp(){
		System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		helenJacksonPage = new HelenJacksonPage(driver);
	}
	
	@Before
	public void toMainPage(){
		helenJacksonPage.getMainPage();
	}
	
	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void checkAnimationSpeedDefaultValue() {
		assertEquals("2000", helenJacksonPage.getAnimationSpeedText());
	}
	
	@Test
	public void checkAnimationSpeedCanBe100() {
		helenJacksonPage.setAnimationSpeedText("100");
		// check anim_ement changes 
	}

	@Test
	public void checkAnimationSpeedCanNotBeText() {
		helenJacksonPage.setAnimationSpeedText("bla-bla");
		assertTrue("Должна быть всплывающая подсказка", helenJacksonPage.getPopupMessage().contains("Пожалуйста, введите число"));
	}
}
