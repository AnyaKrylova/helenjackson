package com.behavox.example.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.behavox.example.page.HelenJacksonPage;
import com.behavox.example.page.TestData;

import static com.behavox.example.page.TestData.PATH_TO_DRIVER;
import static com.behavox.example.page.TestData.MENU_ITEMS;
import static com.behavox.example.page.TestData.SIDEBAR_ITEMS;

public class CheckLinks {
	
	static WebDriver driver;
	static HelenJacksonPage helenJacksonPage;
	
	@BeforeClass
	public static void setUp(){
		System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		helenJacksonPage = new HelenJacksonPage(driver);
	}
	
	@Before
	public void toMainPage(){
		helenJacksonPage.getMainPage();
	}
	
	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void checkMenuItems(){
		List<WebElement> menuItems = helenJacksonPage.getMenuItems();
			
		assertEquals(5, menuItems.size());
		
		for (int i = 0; i < menuItems.size(); i++){
			assertEquals(MENU_ITEMS[i], menuItems.get(i).getText());
		}
	}

	@Test
	public void checkSidebarItems(){
		List<WebElement> sidebarItems = helenJacksonPage.getSidebarItems();
			
		assertEquals(8, sidebarItems.size());
		
		for (int i = 0; i < sidebarItems.size(); i++){
			assertEquals(SIDEBAR_ITEMS[i], sidebarItems.get(i).getText());
		}
	}
	
	@Test
	public void checkMenuItemLinking() {
		HashMap<String, String> menuItems = TestData.setMenuItemsValues();
		
		for (HashMap.Entry<String, String> item : menuItems.entrySet()) {
			helenJacksonPage.getMainPage();
			helenJacksonPage.clickMenuItem(item.getKey());
			Validate.isTrue(checkUrlContainsText(item.getValue()));	
		}
	}
	
	private Boolean checkUrlContainsText(String itemUrl) {
		return (new WebDriverWait(driver, 10))
			.until(ExpectedConditions.urlContains(itemUrl));
	}
	
}
